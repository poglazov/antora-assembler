= La Garita Mountains

The La Garita Mountains are a subrange in the San Juan Mountains.

== La Garita Wilderness

The La Garita Wilderness is located in the La Garita Mountains.
It overlaps areas of the Gunnison National Forest and Rio Grande National Forest.

=== Trails

The wilderness area contains numerous backpacking trails as well as shorter trails, such as the xref:willow-creek.adoc[East Willow Creek trail].

==== Continental Divide Trail

A segment of the Continental Divide Trail is located in the La Garita Mountains.