'use strict'

const produceAggregateDocument = require('@antora/assembler/produce-aggregate-document')
const { expect } = require('chai')
const { promises: fsp } = require('fs')
const loadScenario = require('./load-scenario')
const ospath = require('path')

// Q: does this belong in @antora/assembler/test/test-helper.js?
async function runScenario (name, dirname) {
  const { loadAsciiDoc, dir, componentVersion, contentCatalog, mutableAttributes, asciidocConfig, assemblerConfig } =
    await loadScenario(name, dirname)
  const result = produceAggregateDocument(
    loadAsciiDoc,
    contentCatalog,
    componentVersion,
    componentVersion.navigation[0],
    assemblerConfig.asciidoc.attributes.doctype,
    contentCatalog.getPages(),
    asciidocConfig, // equivalent to mergedAsciiDocConfig
    mutableAttributes,
    assemblerConfig.sectionMergeStrategy
  )
  expect(result).to.exist()
  expect(result).to.have.property('src')
  const expected = await fsp.readFile(ospath.join(dir, 'expects', result.src.basename), 'utf8').catch(() => '')
  expect(result.contents.toString()).to.eql(expected)
  return result
}

module.exports = runScenario
